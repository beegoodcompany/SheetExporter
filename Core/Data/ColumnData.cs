﻿using System.Collections.Generic;

namespace BeeGood.Data
{
    public class ColumnData
    {
        public string ColumnName;
        public List<CellData> TitleCellList = new();
        public List<CellData> RowList = new();
    }
}