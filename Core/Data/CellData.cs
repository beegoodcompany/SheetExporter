﻿namespace BeeGood.Data
{
    public class CellData
    {
        public string Data;
        public int StartRowIndex;
        public int HeightRange;
    }
}