﻿using System.Collections.Generic;
using BeeGood.Base;
using BeeGood.Interfaces;

namespace BeeGood.Data
{
    public class RowModel: BaseModel
    {
        private List<IRowModel> RowValues;

        public string Name;

        public RowModel(string idValue) : base(idValue)
        {
            RowValues = new List<IRowModel>();
        }
 
        public override List<IRowModel> GetRowsInner(string id)
        {
            List<IRowModel> rows = new();
            foreach (var rowInnerValue in GetRowInnerList())
            {
                if (rowInnerValue.IdModel == id)
                {
                    rows.Add(rowInnerValue);
                }
            }

            return rows;
        }
        
        public override List<IRowModel> GetRowInnerList()
        {
            return RowValues;
        }
    }
}