﻿using System.Collections.Generic;
using BeeGood.Interfaces;

namespace BeeGood.Data
{
    public class RowInnerValue: IRowModel
    {
        public string IdModel { get; }
        public List<string> Data;

        public RowInnerValue(string idModel)
        {
            Data = new List<string>();
            IdModel = idModel;
        }
    }
}