﻿using System.Data;
using ExcelDataReader;

namespace BeeGood.Interfaces
{
    public interface ISheetPage
    {
        public string Name { get; }
        public DataTable Table { get; }
        public CellRange[] MergeCells { get; }
    }
}