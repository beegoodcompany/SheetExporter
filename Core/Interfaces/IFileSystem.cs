﻿using System.IO;

namespace BeeGood.Interfaces
{
    public interface IFileSystem
    {
        bool Exists(string path);
        void CreateDirectory(string path);
        
        Stream OpenRead(string path);
        Stream OpenWrite(string path);
    }
}