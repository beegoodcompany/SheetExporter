﻿using System.Threading.Tasks;
using BeeGood.Base;

namespace BeeGood.Interfaces
{
    public interface ISheetConverter: ISheetImporter, ISheetExporter
    {
        
    }
    
    public interface ISheetExporter
    {
        Task<bool> Export(BaseContainer context);
    }
    
    public interface ISheetImporter
    {
        ISheetModel GetModels();
        Task<bool> Import(BaseContainer context);
    }
}