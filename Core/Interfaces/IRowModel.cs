﻿namespace BeeGood.Interfaces
{
    public interface IRowModel
    {
        public string IdModel { get; }
    }
}