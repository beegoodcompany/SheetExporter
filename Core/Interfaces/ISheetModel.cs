﻿using System;
using System.Collections.Generic;
using BeeGood.Data;

namespace BeeGood.Interfaces
{
    public interface ISheetModel
    {
        Dictionary<Type, List<RowModel>> RowModels { get; }   
    }
}