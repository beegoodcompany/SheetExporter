﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace BeeGood
{
    public static class SheetExporterConfig
    {
        public static readonly string JsonExtension = ".json";
        
        public static void SplitNames(string name, out string sheetName, out string subName)
        {
            var splittedNames = name.Split(new char[] { '.' }, 2);
            if (splittedNames.Length < 2)
            {
                sheetName = splittedNames[0];
                subName = null;
                return;
            }
            sheetName = splittedNames[0];
            subName = splittedNames[1];
        }
        
        public static IEnumerable<FieldInfo> GetEligibleFields(Type type)
        {
            const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic |
                                              BindingFlags.Instance | BindingFlags.DeclaredOnly;

            while (type != null)
            {
                var fields = type.GetFields(bindingFlags);

                foreach (var field in fields)
                {
                    if (field.IsDefined(typeof(NonSerializedAttribute)))
                    {
                        continue;
                    }
                    
                    
                    yield return field;
                }

                type = type.BaseType;
            }
        }
        
        public static PropertyInfo GetProperty(IEnumerable<PropertyInfo> propertyTypes, string nameField)
        {
            foreach (var property in propertyTypes)
            {
                if (property.Name == nameField)
                {
                    return property;
                }
            }
            
            return null;
        }
    }
}