﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BeeGood.Base;
using BeeGood.Interfaces;
using ExcelDataReader;

namespace BeeGood.Converters
{
    public class XlsxImporter : ExcelBaseImporter
    {
        private string filePath;
        private Dictionary<string, List<Page>> pages;

        public XlsxImporter(string loadPath)
        {
            filePath = loadPath;
            pages = new Dictionary<string, List<Page>>();
        }

        private class Page: ISheetPage
        {
            public string Name => name;
            public DataTable Table => table;
            public CellRange[] MergeCells => mergeCells;

            private string name;
            private DataTable table;
            public CellRange[] mergeCells;
            public Page(DataTable table, string name, CellRange[] mergeCells)
            {
                this.table = table;
                this.name = name;
                this.mergeCells = mergeCells;
            }
        }

        protected override Task<bool> LoadFromDocument()
        {
            pages.Clear();
            
            //TODO: Gabbasov Denis check if the file is open
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        UseColumnDataType = false,
                        ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = false
                        }
                    });
                    
                    for (int i = 0; i < result.Tables.Count; i++)
                    {
                        var table = result.Tables[i];
                        var tableName = table.TableName;
                        
                        var mergeCells = reader.MergeCells;
                        
                        SheetExporterConfig.SplitNames(tableName, out var sheetName, out var subName);

                        if (pages.TryGetValue(sheetName, out var pagesList) == false)
                        {
                            pagesList = new List<Page>();
                            pages.Add(sheetName, pagesList);
                        }
                        
                        pagesList.Add(new Page(table, subName, mergeCells));
                    }
                }
            }

            return Task.FromResult(true);
        }

        protected override IEnumerable<ISheetPage> GetPages(string sheetName)
        {
            if (pages.TryGetValue(sheetName, out var value))
                return value;

            return Enumerable.Empty<ISheetPage>();
        }
    }
}