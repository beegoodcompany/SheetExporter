﻿using System;
using System.IO;
using System.Threading.Tasks;
using BeeGood.Base;
using BeeGood.FileSystems;
using BeeGood.Interfaces;
using Newtonsoft.Json;
using UnityEngine;

namespace BeeGood.Converters
{
    public class SheetJsonConverter : ISheetConverter
    {
        private readonly string filePath;
        private readonly string nameFile;
        private readonly IFileSystem fileSystem;

        public SheetJsonConverter(string path, string nameJson)
        {
            filePath = path;
            nameFile = nameJson;
            fileSystem = new FileProvider();
        }

        public ISheetModel GetModels()
        {
            return null;
        }

        protected virtual JsonSerializerSettings GetSettings()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            };

            //TODO: Gabbasov Denis add ILogger and ContractResolver
            return settings;
        }

        public virtual Task<bool> Import(BaseContainer context)
        {
            return Task.FromResult(false);
        }

        public async Task<bool> Export(BaseContainer context)
        {
            var containerPropertiesInfo = context.GetSheetProperties();

            fileSystem.CreateDirectory(filePath);

            if (containerPropertiesInfo.Count > 1)
            {
                Debug.LogWarning($"Only one file has been serialized in class {context.GetType().Name}.");
            }

            foreach (var propertyInfo in containerPropertiesInfo)
            {
                var listData = propertyInfo.Value.GetValue(context);
                if (listData == null)
                {
                    return false;
                }
                var serializedData = Serialize(listData, propertyInfo.Value.FieldType, GetSettings());

                var path = Path.Combine(filePath, $"{nameFile}{SheetExporterConfig.JsonExtension}");

                using (var stream = fileSystem.OpenWrite(path))
                using (var writer = new StreamWriter(stream))
                {
                    await writer.WriteAsync(serializedData);
                }

                break;
            }

            return true;
        }

        protected virtual string Serialize(object value, Type type, JsonSerializerSettings settings)
        {
            return JsonConvert.SerializeObject(value, type, settings);
        }

        protected virtual object Deserialize(string value, Type type, JsonSerializerSettings settings)
        {
            return JsonConvert.DeserializeObject(value, type, settings);
        }
    }
}