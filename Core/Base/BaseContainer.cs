﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BeeGood.Data;
using BeeGood.Interfaces;
using UnityEngine;

namespace BeeGood.Base
{
    public abstract class BaseContainer
    {
        private Dictionary<string, FieldInfo> sheetProperties;

        public async Task<bool> Store(ISheetExporter converter)
        {
            var result = await converter.Export(this);
            return result;
        }

        public IReadOnlyDictionary<string, FieldInfo> GetSheetProperties()
        {
            if (sheetProperties == null)
            {
                var eligibleProperties = SheetExporterConfig.GetEligibleFields(GetType());

                var sheetProperties = new List<FieldInfo>();

                foreach (var propertyInfo in eligibleProperties)
                {
                    if (propertyInfo.FieldType.IsGenericType == false)
                    {
                        Debug.LogError($"Property in class {GetType().Name} with name " +
                                       $"<{propertyInfo.Name}> is not inherited by {typeof(List<>)}. " +
                                       $"It was skipped.");
                        continue;
                    }

                    if (propertyInfo.FieldType.GetGenericTypeDefinition() != typeof(List<>))
                    {
                        continue;
                    }

                    sheetProperties.Add(propertyInfo);
                }

                this.sheetProperties = sheetProperties.ToDictionary(x => x.Name);
            }

            return sheetProperties;
        }

        public async Task<bool> Bake(ISheetImporter importer)
        {
            foreach (var fieldInfo in GetSheetProperties().Values)
            {
                fieldInfo.SetValue(this, null);
            }

            var result = await importer.Import(this);

            if (result == false)
            {
                return false;
            }

            foreach (var fieldInfo in GetSheetProperties().Values)
            {
                var rows = importer.GetModels().RowModels;
                var genericFieldType = fieldInfo.FieldType.GetGenericArguments().Single();

                if (rows.TryGetValue(genericFieldType, out var rowModelsList) == false)
                {
                    Debug.LogError($"The ColumnListData for this type: {fieldInfo.FieldType} could not be found.");
                    continue;
                }

                var dataList = ParseRowsData(fieldInfo.FieldType, rowModelsList);
                fieldInfo.SetValue(this, dataList);
            }

            OnPostBake();

            return true;
        }

        protected virtual void OnPostBake()
        {
        }

        private object ParseValue(Type valueType, string value)
        {
            if (valueType == typeof(float))
            {
                return float.Parse(value, CultureInfo.InvariantCulture);
            }
            
            if(valueType.IsEnum)
            {
                return Enum.Parse(valueType, value);
            }
                        
            return Convert.ChangeType(value, valueType);
        }

        private void ParseInnerRow(FieldInfo propertyInfo, Type propertyType, object createdTypeData, string propertyName, IRowModel baseRowModel)
        {
            if (baseRowModel is RowModel rowModel)
            {
                var propertyDataList = (IList)propertyInfo.GetValue(createdTypeData) ?? (IList)Activator.CreateInstance(propertyType);
                var data = ParseRowsData(propertyType, new List<RowModel>
                {
                    rowModel
                });

                foreach (var propertyObject in data)
                {
                    propertyDataList.Add(propertyObject);
                }

                propertyInfo.SetValue(createdTypeData, propertyDataList);
                return;
            }

            if (baseRowModel is not RowInnerValue rowInner)
            {
                Debug.LogError($"Cannot parse {propertyName}. Not valid model type");
                return;
            }

            var rowInnerDataCount = rowInner.Data.Count;
            if (rowInnerDataCount == 0)
            {
                propertyInfo.SetValue(createdTypeData, null);
                return;
            }

            var isGenericPropertyType = propertyType.IsGenericType;
            // fill list property
            if (isGenericPropertyType)
            {
                var propertyDataList = (IList)Activator.CreateInstance(propertyType);
                foreach (var value in rowInner.Data)
                {
                    if (value != null)
                    {
                        var valueType = propertyType.GetGenericArguments().Single();
                        var changedValue = ParseValue(valueType, value);
                        propertyDataList.Add(changedValue);
                    }
                    else
                    {
                        propertyDataList.Add(null);
                    }
                }

                propertyInfo.SetValue(createdTypeData, propertyDataList);
                return;
            }
            
            // fill single property
            if (rowInnerDataCount > 1)
            {
                Debug.LogWarning($"Data with column name {rowInner.IdModel} has multiple values, but the field is not {typeof(List<>)}.");
            }

            var singleValue = rowInner.Data[0];
            if (singleValue != null)
            {
                var changedValue = ParseValue(propertyType, singleValue);
                propertyInfo.SetValue(createdTypeData, changedValue);
            }
            else
            {
                propertyInfo.SetValue(createdTypeData, null);
            }
        }

        private IList ParseRowsData(Type genericType, List<RowModel> rowModels)
        {
            var typeDataList = (IList)Activator.CreateInstance(genericType);
            var type = genericType.GetGenericArguments().Single();

            // fill values in createdType
            foreach (var row in rowModels)
            {
                var createdTypeData = Activator.CreateInstance(type);
                var createdTypeProperties = SheetExporterConfig.GetEligibleFields(createdTypeData.GetType())
                    .ToList();

                try
                {
                    foreach (var fieldInfo in createdTypeProperties)
                    {
                        var propertyName = fieldInfo.Name;
                        var propertyType = fieldInfo.FieldType;
                        var innerRows = row.GetRowsInner(propertyName);

                        if (innerRows.Count == 0)
                        {
                            continue;
                        }

                        foreach (var baseRowModel in innerRows)
                        {
                            ParseInnerRow(fieldInfo, propertyType, createdTypeData, propertyName, baseRowModel);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError($"ERROR at row `{row.Name}` with message: {e}");
                    return null;
                }

                typeDataList.Add(createdTypeData);
            }

            return typeDataList;
        }
    }
}