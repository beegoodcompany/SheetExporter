﻿using System.Collections.Generic;
using BeeGood.Interfaces;

namespace BeeGood.Base
{
    public abstract class BaseModel : IRowModel
    {
        public abstract List<IRowModel> GetRowInnerList();
        public abstract List<IRowModel> GetRowsInner(string id);
        public string IdModel { get; }

        public BaseModel(string idModel)
        {
            IdModel = idModel;
        }
    }
}