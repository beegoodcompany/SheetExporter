﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using BeeGood.Data;
using BeeGood.Interfaces;
using UnityEngine;

namespace BeeGood.Base
{
    public abstract class ExcelBaseImporter : ISheetImporter, ISheetModel
    {
        protected abstract Task<bool> LoadFromDocument();
        protected abstract IEnumerable<ISheetPage> GetPages(string sheetName);

        public Dictionary<Type, List<RowModel>> RowModels { get; } = new();

        public ISheetModel GetModels()
        {
            return this;
        }

        public virtual async Task<bool> Import(BaseContainer context)
        {
            var result = await LoadFromDocument();
            if (result == false)
            {
                Debug.LogError("Failed load data from document");
                return false;
            }

            foreach (var fieldInfo in context.GetSheetProperties())
            {
                var key = fieldInfo.Key.Replace("<", "").Replace(">", "").Replace("k__BackingField", "");
                var pages = GetPages(key);
                var type = fieldInfo.Value.FieldType.GetGenericArguments().Single();

                foreach (var page in pages)
                {
                    ImportData(page, type);
                }
            }

            return true;
        }

        private void ImportData(ISheetPage page, Type type)
        {
            var rowModels = ParsePage(page);

            //TODO: Gabbasov Denis add data if there are more than one pages
            if (RowModels.TryGetValue(type, out var dataList) == false)
            {
                dataList = rowModels;
                RowModels.Add(type, dataList);
            }
        }

        private List<RowModel> ParsePage(ISheetPage page)
        {
            Dictionary<int, ColumnData> columnList = new();
            var rows = page.Table.Rows;

            int maxRow = GetHeightCellRange(rows, 0, rows.Count, 0) - 1;
            for (int i = 0; i < page.Table.Columns.Count; i++)
            {
                var columnData = ParseColumn(columnList, i, rows, maxRow);
                if (columnData == null)
                {
                    continue;
                }

                columnList.Add(i, columnData);
            }

            // checking if indexes are out of bounds
            var columnId = columnList[0].RowList;
            foreach (var row in columnId)
            {
                var rowStart = row.StartRowIndex;
                var rowEnd = row.StartRowIndex + row.HeightRange - 1;
                for (int i = 1; i < columnList.Count; i++)
                {
                    var columnRows = columnList[i].RowList;
                    foreach (var columnRow in columnRows)
                    {
                        if (columnRow.StartRowIndex < rowStart || columnRow.StartRowIndex > rowEnd)
                        {
                            continue;
                        }

                        var columnRowEnd = columnRow.StartRowIndex + columnRow.HeightRange - 1;
                        if (columnRowEnd > rowEnd)
                        {
                            var offset = columnRowEnd - rowEnd;
                            columnRow.HeightRange -= offset;
                        }
                    }
                }
            }

            var rowModels = GroupModels(columnList);

            return rowModels;
        }

        private List<RowModel> GroupModels(Dictionary<int, ColumnData> columnList)
        {
            var rowModels = new List<RowModel>();

            var columnId = columnList[0];
            var columnValuesList = columnList.Values.ToList();

            Dictionary<ColumnData, List<CellData>> groupedCellsByColumn = new();
            foreach (var columnData in columnValuesList)
            {
                groupedCellsByColumn.Add(columnData, columnData.RowList);
            }
            
            foreach (var cell in columnId.RowList)
            {
                var rowStart = cell.StartRowIndex;
                var rowEnd = cell.StartRowIndex + cell.HeightRange - 1;

                RowModel rowModel = new RowModel(columnId.ColumnName)
                {
                    Name = cell.Data
                };

                var idRowModelList = rowModel.GetRowInnerList();
                
                var groupedCellsCurrentCellId = GetCellsByHeightRange(groupedCellsByColumn,
                    rowStart, rowEnd);
                List<string> parsedColumns = new();
                foreach (var kv in groupedCellsCurrentCellId)
                {
                    var column = kv.Key;
                    if (column.TitleCellList.Count > 1)
                    {
                        if (parsedColumns.Contains(column.ColumnName))
                        {
                            continue;
                        }

                        parsedColumns.Add(column.ColumnName);
                        var model = GetModelsByColumn(cell.Data, 0, column, groupedCellsCurrentCellId, parsedColumns);
                        idRowModelList.AddRange(model);
                    }
                    else
                    {
                        RowInnerValue rowInnerValue = new RowInnerValue(column.ColumnName);
                        foreach (var cellData in kv.Value)
                        {
                            rowInnerValue.Data.Add(cellData.Data);
                        }
                        
                        idRowModelList.Add(rowInnerValue);
                    }
                }

                rowModels.Add(rowModel);
            }

            return rowModels;
        }

        private List<RowModel> GetModelsByColumn(string parentName, int titleIndex, ColumnData columnData, Dictionary<ColumnData, List<CellData>> groupedCellsByColumn, List<string> parsedColumnsList)
        {
            var titleCell = columnData.TitleCellList[titleIndex];
            var idCells = groupedCellsByColumn[columnData];
            var rowModels = new List<RowModel>(idCells.Count);

            foreach (var idCell in idCells)
            {
                var rowHeightStart = idCell.StartRowIndex;
                var rowHeightEnd = idCell.StartRowIndex + idCell.HeightRange - 1;
                
                var groupedByCellId = GetCellsByHeightRange(groupedCellsByColumn,
                    rowHeightStart, rowHeightEnd, titleCell);

                var innerRowModel = new RowModel(titleCell.Data)
                {
                    Name = idCell.Data
                };
                var innerRowModels = innerRowModel.GetRowInnerList();
                
                var idLastCellName = columnData.TitleCellList[^1].Data;
                RowInnerValue rowInnerId = new RowInnerValue(idLastCellName);
                rowInnerId.Data.Add(idCell.Data);
                innerRowModels.Add(rowInnerId);

                var parsedColumnsInnerList = new List<string>(parsedColumnsList);
                foreach (var kv in groupedByCellId)
                {
                    if (kv.Key == columnData)
                    {
                        continue;
                    }
                    
                    var column = kv.Key;
                    var titleLastCellName = column.TitleCellList[^1].Data;
                    if (column.TitleCellList.Count - 1 > titleIndex + 1)
                    {
                        var nextTitleCellName = column.TitleCellList[titleIndex + 1].Data;
                        var fullName = $"{nextTitleCellName}.{idCell.Data}";
                        
                        if (parsedColumnsInnerList.Contains(fullName))
                        {
                            continue;
                        }
                        
                        parsedColumnsInnerList.Add(fullName);
                        var model = GetModelsByColumn(idCell.Data, titleIndex + 1, column, groupedByCellId, parsedColumnsInnerList);
                        innerRowModels.AddRange(model);
                    }
                    else
                    {
                        RowInnerValue rowInnerValue = new RowInnerValue(titleLastCellName);
                        foreach (var cellData in kv.Value)
                        {
                            rowInnerValue.Data.Add(cellData.Data);
                        }
                        
                        innerRowModels.Add(rowInnerValue);
                    }
                }
                
                rowModels.Add(innerRowModel);
            }

            return rowModels;
        }
        
        private Dictionary<ColumnData, List<CellData>> GetCellsByHeightRange(Dictionary<ColumnData, List<CellData>> groupedCellsByColumn,
            int rowHeightStart, int rowHeightEnd, CellData includeCellData = null)
        {
            var rowInnerList = new Dictionary<ColumnData, List<CellData>>();

            foreach (var kv in groupedCellsByColumn)
            {
                if (includeCellData != null)
                {
                    CellData cellData = null;
                    foreach (var titleCell in kv.Key.TitleCellList)
                    {
                        if (titleCell == includeCellData)
                        {
                            cellData = titleCell;
                            break;
                        }
                    }

                    if (cellData == null)
                    {
                        continue;
                    }
                }

                rowInnerList.Add(kv.Key, new());
                
                foreach (var cell in kv.Value)
                {
                    var cellStart = cell.StartRowIndex;
                    var cellEnd = cellStart + cell.HeightRange - 1;
                    
                    if (cellStart <= rowHeightEnd && cellEnd >= rowHeightStart)
                    {
                        rowInnerList[kv.Key].Add(cell);
                    }
                }
            }

            return rowInnerList;
        }

        private string FindColumnName(Dictionary<int, ColumnData> prevColumns, DataRowCollection rows, int columnIndex, int startRowIndex, out CellData cellData)
        {
            var rowName = $"{rows[startRowIndex][columnIndex]}";
            if (!string.IsNullOrEmpty(rowName))
            {
                cellData = prevColumns.TryGetValue(columnIndex, out var column) ? 
                    column.TitleCellList[startRowIndex] : null;

                return rowName;
            }

            for (int i = columnIndex - 1; i > 0; i--)
            {
                if (!prevColumns.ContainsKey(i))
                {
                    continue;
                }

                var prevColumn = prevColumns[i];
                var prevSubCell = prevColumn.TitleCellList[startRowIndex];
                
                cellData = prevSubCell;
                if (string.IsNullOrEmpty(prevSubCell.Data))
                {
                    break;
                }

                return prevSubCell.Data;
            }

            cellData = null;
            return string.Empty;
        }

        private int GetHeightCellRange(DataRowCollection rows, int cellRowIndex, int rowsCount, int columnIndex)
        {
            int range = 1;
            for (int j = cellRowIndex + 1; j < rowsCount; j++)
            {
                var nextRow = rows[j];
                var nextRowData = $"{nextRow[columnIndex]}";
                if (!string.IsNullOrEmpty(nextRowData))
                {
                    break;
                }

                range++;
            }

            return range;
        }
        
        private ColumnData ParseColumn(Dictionary<int, ColumnData> prevColumns, int columnIndex, DataRowCollection rows, int maxRow)
        {
            var columnName = FindColumnName(prevColumns, rows, columnIndex, 0, out _);
            if (string.IsNullOrEmpty(columnName))
            {
                Debug.LogWarning($"Empty column with id: {columnIndex + 1} was skipped.");
                return null;
            }

            ColumnData columnData = new ColumnData
            {
                ColumnName = columnName,
                TitleCellList = new()
            };

            var rowsCount = rows.Count;
            var rowHeight = 0;
            
            for (int i = 0; i <= maxRow; i++)
            {
                var subColumnName = FindColumnName(prevColumns, rows, columnIndex, i, out var cellData);
                if (string.IsNullOrEmpty(subColumnName))
                {
                    break;
                }
                
                if (cellData == null)
                {
                    var heightRange = GetHeightCellRange(rows, i, rowsCount, columnIndex);
                    if (heightRange > maxRow)
                    {
                        heightRange = maxRow - i + 1;
                    }
                    
                    cellData = new CellData
                    {
                        Data = subColumnName,
                        StartRowIndex = i,
                        HeightRange = heightRange,
                    };
                }
                
                columnData.TitleCellList.Add(cellData);

                rowHeight += cellData.HeightRange;
                if (rowHeight - 1 >= maxRow)
                {
                    break;
                }
            }

            for (int i = maxRow + 1; i < rowsCount; i++)
            {
                var row = rows[i];
                var rowData = $"{row[columnIndex]}";

                if (string.IsNullOrEmpty(rowData))
                {
                    continue;
                }
                
                var heightRange = GetHeightCellRange(rows, i, rowsCount, columnIndex);
                columnData.RowList.Add(new CellData
                {
                    Data = rowData,
                    StartRowIndex = i,
                    HeightRange = heightRange
                });
            }

            return columnData;
        }
    }
}