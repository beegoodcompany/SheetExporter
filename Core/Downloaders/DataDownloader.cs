﻿using System.IO;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using UnityEngine;

namespace BeeGood.Downloaders
{
    public class DataDownloader
    {
        private readonly string mimeTypeSheet = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly string credentialPath;
        private readonly string saveTo;
        
        private MemoryStream memoryStream;

        public DataDownloader(string folderToSave, string credentialPath)
        {
            saveTo = folderToSave;
            this.credentialPath = credentialPath;
        }

        public async Task<string> Download(string sheetId)
        {
            memoryStream = new MemoryStream();

            var googleCredential = await File.ReadAllTextAsync(credentialPath);

            var credential = GoogleCredential.FromJson(googleCredential)
                .CreateScoped(DriveService.Scope.DriveReadonly);

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });

            FilesResource.GetRequest getRequest = service.Files.Get(sheetId);
            string sheetName = getRequest.Execute().Name;

            var pathToSave = Path.Combine(Application.dataPath, $"{saveTo}/{sheetName}.xlsx");

            FilesResource.ExportRequest exportRequest = service.Files.Export(sheetId, mimeTypeSheet);

            var downloadProgress = await exportRequest.DownloadAsync(memoryStream);
            if (downloadProgress.Status != DownloadStatus.Completed)
            {
                Debug.LogError($"Download failed.");
                return null;
            }

            await using (FileStream file = new FileStream(pathToSave, FileMode.Create, FileAccess.Write))
            {
                memoryStream.WriteTo(file);
                return pathToSave;
            }
        }
    }
}