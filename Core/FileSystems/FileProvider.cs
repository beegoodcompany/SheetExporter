﻿using System.IO;
using BeeGood.Interfaces;

namespace BeeGood.FileSystems
{
    public class FileProvider: IFileSystem
    {
        public bool Exists(string path)
        {
            return File.Exists(path);
        }

        public void CreateDirectory(string path)
        {
            Directory.CreateDirectory(path);
        }

        public Stream OpenRead(string path)
        {
            return File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        }

        public Stream OpenWrite(string path)
        {
            return File.Open(path, FileMode.Create, FileAccess.Write, FileShare.Read);
        }
    }
}