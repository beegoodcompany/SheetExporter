﻿using System;
using System.Collections.Generic;

namespace BeeGood.Window
{
    [Serializable]
    public class ExporterUISheets
    {
        public List<ExporterUISheet> UISheets;
        public string LastSelectedUISheetName;
        
        public ExporterUISheet GetUiSheetByName(string name)
        {
            foreach (var uiSheet in UISheets)
            {
                if (uiSheet.NameSheet == name)
                {
                    return uiSheet;
                }
            }

            return null;
        }
    }
}