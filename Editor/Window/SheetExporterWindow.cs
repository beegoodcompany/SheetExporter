﻿using System;
using System.Collections.Generic;
using System.IO;
using BeeGood.Base;
using BeeGood.Converters;
using BeeGood.Downloaders;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;
using UnityEngine.UIElements;
using Assembly = System.Reflection.Assembly;

namespace BeeGood.Window
{
    public class SheetExporterWindow : EditorWindow
    {
        private const string ContainersPath = "Editor/SheetExporter/Containers";
        private const string CredentialPath = "Editor/SheetExporter/StratagemmaCredential.json";
        private const string ExcelPath = "Editor/SheetExporter/Excel";
        private const string TreePath = "Packages/com.beegood.sheetexporter/Editor/UXML/SheetEditor.uxml";
        
        private VisualTreeAsset tree = null;
        
        private ExporterUISheets savedUiSheets;
        private bool isExporting = false;

        private DropdownField dropdownFieldSheets;

        private TextField nameSheetTextField;
        private TextField sheetIdTextField;
        private TextField nameJsonTextField;
        private TextField nameFolderSaveToTextField;
        private TextField parserTypeClassTextField;

        private Button buttonStartExport;
        private Button buttonSaveSheet;
        private Button buttonAddSheet;
        private Button buttonRemoveSheet;

        [MenuItem("Tools/SheetExporter/Google Sheet Export")]
        public static void ShowEditor()
        {
            GetWindow<SheetExporterWindow>("Google Sheet Exporter");
        }

        public void CreateGUI()
        {
            if (tree == null)
            {
                var templateUxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(TreePath);
                if (templateUxml == null)
                {
                    Debug.LogError($"Failed to load SheetEditor.uxml in path {TreePath}");
                    return;
                }

                tree = templateUxml;
            }
            
            savedUiSheets = new ExporterUISheets
            {
                UISheets = new List<ExporterUISheet>()
            };

            tree.CloneTree(rootVisualElement);

            dropdownFieldSheets = rootVisualElement.Q<DropdownField>("DropdownSheets");

            nameSheetTextField = rootVisualElement.Q<TextField>("TextFieldNameSheet");
            sheetIdTextField = rootVisualElement.Q<TextField>("TextFieldSheetId");
            nameJsonTextField = rootVisualElement.Q<TextField>("TextFieldNameJSON");
            nameFolderSaveToTextField = rootVisualElement.Q<TextField>("TextFieldNameFolderTo");
            parserTypeClassTextField = rootVisualElement.Q<TextField>("TextFieldParserTypeClass");

            buttonStartExport = rootVisualElement.Q<Button>("ButtonStartExport");
            buttonSaveSheet = rootVisualElement.Q<Button>("ButtonSaveSheet");
            buttonAddSheet = rootVisualElement.Q<Button>("ButtonAddSheet");
            buttonRemoveSheet = rootVisualElement.Q<Button>("ButtonRemoveSheet");

            LoadSavedUISheets();

            dropdownFieldSheets.RegisterValueChangedCallback(OnChangeValueDropDownField);

            buttonStartExport.clicked += DownloadFromGoogle;
            buttonSaveSheet.clicked += SaveSelectedSheet;
            buttonAddSheet.clicked += AddNewSheet;
            buttonRemoveSheet.clicked += RemoveSheet;
        }

        private void OnChangeValueDropDownField(ChangeEvent<string> evt)
        {
            savedUiSheets.LastSelectedUISheetName = evt.newValue;
            var uiSheet = savedUiSheets.GetUiSheetByName(evt.newValue);

            if (uiSheet == null)
            {
                return;
            }

            SetDataInTextFields(uiSheet);

            SaveSelectedSheet();
        }

        private void SetDataInTextFields(ExporterUISheet uiSheet)
        {
            nameSheetTextField.value = uiSheet.NameSheet;
            sheetIdTextField.value = uiSheet.SheetID;
            nameJsonTextField.value = uiSheet.NameJson;
            nameFolderSaveToTextField.value = uiSheet.SaveJsonTo;
            parserTypeClassTextField.value = uiSheet.ParserTypeClass;
        }

        private void AddNewSheet()
        {
            string containersPath = Path.Combine(Application.dataPath, ContainersPath);
            if (File.Exists(containersPath) == false)
            {
                Directory.CreateDirectory(containersPath);
            }
            string excelPath = Path.Combine(Application.dataPath, ExcelPath);
            if (File.Exists(excelPath) == false)
            {
                Directory.CreateDirectory(excelPath);
            }
            
            var newUiSheet = new ExporterUISheet();

            newUiSheet.NameSheet += savedUiSheets.UISheets.Count;

            savedUiSheets.UISheets.Add(newUiSheet);

            dropdownFieldSheets.choices.Add(newUiSheet.NameSheet);
            dropdownFieldSheets.value = newUiSheet.NameSheet;
        }

        private void RemoveSheet()
        {
            var uiSheet = savedUiSheets.GetUiSheetByName(dropdownFieldSheets.value);
            savedUiSheets.UISheets.Remove(uiSheet);

            nameSheetTextField.value = "";
            sheetIdTextField.value = "";
            nameJsonTextField.value = "";
            nameFolderSaveToTextField.value = "";
            parserTypeClassTextField.value = "";

            var indexDropDownField = dropdownFieldSheets.index;
            dropdownFieldSheets.index = -1;
            dropdownFieldSheets.choices.Remove(dropdownFieldSheets.choices[indexDropDownField]);
            dropdownFieldSheets.value = "";
        }

        private void SaveSelectedSheet()
        {
            if (dropdownFieldSheets.index != -1)
            {
                var uiSheet = savedUiSheets.GetUiSheetByName(dropdownFieldSheets.value);

                if (uiSheet == null)
                {
                    return;
                }

                var uiSheetName = nameSheetTextField.value;
                var indexDropDownField = dropdownFieldSheets.index;

                dropdownFieldSheets.choices[indexDropDownField] = uiSheetName;
                dropdownFieldSheets.value = uiSheetName;
                savedUiSheets.LastSelectedUISheetName = uiSheetName;

                uiSheet.SetData(uiSheetName, sheetIdTextField.value, nameJsonTextField.value,
                    nameFolderSaveToTextField.value, parserTypeClassTextField.value);
            }

            var jsonConverter = new UISheetJsonConverter();
            jsonConverter.Save(savedUiSheets);

            AssetDatabase.Refresh();
        }

        private void LoadSavedUISheets()
        {
            var jsonConverter = new UISheetJsonConverter();
            var data = jsonConverter.Load();

            if (data.UISheets == null)
            {
                return;
            }

            savedUiSheets = data;

            foreach (var uiSheet in savedUiSheets.UISheets)
            {
                dropdownFieldSheets.choices.Add(uiSheet.NameSheet);
            }

            var lastSelectedUiSheet = savedUiSheets.GetUiSheetByName(savedUiSheets.LastSelectedUISheetName);

            if (lastSelectedUiSheet == null)
            {
                return;
            }

            dropdownFieldSheets.value = lastSelectedUiSheet.NameSheet;
            SetDataInTextFields(lastSelectedUiSheet);
        }

        private async void DownloadFromGoogle()
        {
            if (isExporting)
            {
                return;
            }
            
            var uiSheet = savedUiSheets.GetUiSheetByName(dropdownFieldSheets.value);
            if (uiSheet == null)
            {
                return;
            }
            
            string xlsxFolder = Path.Combine(Application.dataPath, ExcelPath);
            if (File.Exists(xlsxFolder) == false)
            {
                Directory.CreateDirectory(xlsxFolder);
            }
            string credentialPath = Path.Combine(Application.dataPath, CredentialPath);
            if (File.Exists(credentialPath) == false)
            {
                Debug.LogError($"Could not find credential in path {credentialPath}");
                return;
            }
            
            string googleSheetId = uiSheet.SheetID;
            isExporting = true;
            buttonStartExport.SetEnabled(false);
            
            var downloader = new DataDownloader(xlsxFolder, credentialPath);
            
            var result = await downloader.Download(googleSheetId);
            if (result == null)
            {
                isExporting = false;
                buttonStartExport.SetEnabled(true);
                return;
            }
            
            ConvertFromExcel(result);
            
            AssetDatabase.Refresh();
        }

        private async void ConvertFromExcel(string excelPath)
        {
            var uiSheet = savedUiSheets.GetUiSheetByName(dropdownFieldSheets.value);
            if (uiSheet == null)
            {
                isExporting = false;
                buttonStartExport.SetEnabled(true);
                return;
            }
            
            string jsonSavePath = Path.Combine(Application.dataPath, uiSheet.SaveJsonTo);
            string className = uiSheet.ParserTypeClass;
            string jsonName = uiSheet.NameJson;

            string containersPath = Path.Combine(Application.dataPath, ContainersPath);
            if (File.Exists(containersPath) == false)
            {
                Directory.CreateDirectory(containersPath);
            }

            var classFile = $"{className}.cs";
            var assemblies = CompilationPipeline.GetAssemblies();
            string assemblyName = "";
            foreach (var assembly in assemblies)
            {
                bool isCorrectAssembly = false;
                foreach (var sourceFile in assembly.sourceFiles)
                {
                    if (Path.GetFileName(sourceFile) == classFile)
                    {
                        isCorrectAssembly = true;
                        assemblyName = assembly.name;
                        break;
                    }
                }

                if (isCorrectAssembly)
                {
                    break;
                }
            }

            if (string.IsNullOrEmpty(assemblyName))
            {
                isExporting = false;
                buttonStartExport.SetEnabled(true);
                Debug.LogError("Parser Type Class not found.", this);
                return;
            }
            
            var loadedTypes = Assembly.Load(assemblyName).GetTypes();
            Type typeContainer = null;
            foreach (var loadedType in loadedTypes)
            {
                if (loadedType.Name == className)
                {
                    typeContainer = loadedType;
                    break;
                }
            }
            
            if (typeContainer == null)
            {
                isExporting = false;
                buttonStartExport.SetEnabled(true);
                Debug.LogError("Parser Type Class not found.", this);
                return;
            }
            
            var instanceContainer = (BaseContainer)Activator.CreateInstance(typeContainer);
            
            var xlsxConverter = new XlsxImporter(excelPath);
            var jsonConverter = new SheetJsonConverter(jsonSavePath, jsonName);
            
            await instanceContainer.Bake(xlsxConverter);
            await instanceContainer.Store(jsonConverter);
            
            isExporting = false;
            buttonStartExport.SetEnabled(true);
            
            Debug.Log($"Config Export to `{jsonSavePath}` with json name `{jsonName}` successful");
        }
    }
}