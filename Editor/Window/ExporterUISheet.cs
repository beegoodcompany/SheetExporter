﻿using System;

namespace BeeGood.Window
{
    [Serializable]
    public class ExporterUISheet
    {
        public string NameSheet = "NameSheet";
        public string SheetID = "SheetId";
        public string NameJson = "NameJson";
        public string SaveJsonTo = "FolderTo";
        public string ParserTypeClass = "TypeClass";

        public void SetData(string NameSheet, string SheetID, string NameJson, string SaveJsonTo,  
            string ParserTypeClass)
        {
            this.NameSheet = NameSheet;
            this.SheetID = SheetID;
            this.NameJson = NameJson;
            this.SaveJsonTo = SaveJsonTo;   
            this.ParserTypeClass = ParserTypeClass;
        }
            
    }
}