﻿using System.IO;
using UnityEngine;

namespace BeeGood.Window
{
    public class UISheetJsonConverter
    {
        private readonly string nameFolder = "Editor/SheetExporter";
        private readonly string nameJson = "SavedUISheets.json";
        private string path;
        
        public UISheetJsonConverter()
        {
            path = Path.Combine(Application.dataPath, nameFolder);
        }
        
        public void Save(ExporterUISheets sheets)
        {
            string json = JsonUtility.ToJson(sheets);
            if (File.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
            File.WriteAllText(Path.Combine(path, nameJson), json);
        }

        public ExporterUISheets Load()
        {
            var jsonPath = Path.Combine(path, nameJson);
            
            if (File.Exists(jsonPath) == false)
            {
                return new ExporterUISheets();
            }
            
            string json = File.ReadAllText(jsonPath);
            
            var data = JsonUtility.FromJson<ExporterUISheets>(json);
            return data;
        }
    }
}